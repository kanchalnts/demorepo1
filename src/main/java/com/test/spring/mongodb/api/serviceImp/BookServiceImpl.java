package com.test.spring.mongodb.api.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.spring.mongodb.api.model.Book;
import com.test.spring.mongodb.api.repository.MongoRepo;

import com.test.spring.mongodb.api.service.BookService;

@Service
public class BookServiceImpl implements BookService {
	
	@Autowired(required=true)
	private MongoRepo mongoRepo;

	@Override
	public Book deletebookByName(String bookName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Book deleteById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> getAllBookDetails() {

		return mongoRepo.findAll();

	}

	@Override
	public Book findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveBook(Book book) {
		mongoRepo.save(book);
		
	}

	
	

}
