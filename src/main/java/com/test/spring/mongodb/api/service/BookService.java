package com.test.spring.mongodb.api.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.test.spring.mongodb.api.model.Book;

@Service
public interface BookService {


	public Book deletebookByName(String bookName);

	public Book deleteById(int id);

	public List<Book> getAllBookDetails();

	public Book findById(int id);

	public void saveBook(Book book);

}
