package com.test.spring.mongodb.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.test.spring.mongodb.api.model.Book;

@Repository
public interface MongoRepo extends MongoRepository<Book, Integer> {
	

}
