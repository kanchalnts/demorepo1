/*
 * package com.test.spring.mongodb.api.controller;
 * 
 * import java.util.List;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.web.bind.annotation.DeleteMapping; import
 * org.springframework.web.bind.annotation.GetMapping; import
 * org.springframework.web.bind.annotation.ModelAttribute; import
 * org.springframework.web.bind.annotation.PathVariable; import
 * org.springframework.web.bind.annotation.PostMapping; import
 * org.springframework.web.bind.annotation.RequestBody; import
 * org.springframework.web.bind.annotation.RequestMapping; import
 * org.springframework.web.bind.annotation.RestController;
 * 
 * import com.test.spring.mongodb.api.model.Book; import
 * com.test.spring.mongodb.api.service.BookService;
 * 
 * @RestController //@RequestMapping("/book") public class BookController {
 * 
 * @Autowired private BookService bookService;
 * 
 * @PostMapping("/addBook") public String saveBook(@RequestBody Book book) {
 * 
 * bookService.saveBook(book); return "Added Book with id : " + book.getId(); }
 * 
 * @GetMapping("/findAllBooks") public List<Book> getBooks() { return null; }
 * 
 * @GetMapping("/bookdetails")
 * 
 * public List<Book> getAllBooksDetails() { return
 * bookService.getAllBookDetails(); }
 * 
 * 
 * @GetMapping("/findAllBooks/{id}") public Optional<Book> getBook(@PathVariable
 * int id) { //return repository.findById(id); bookService.fi }
 * 
 * 
 * @DeleteMapping("/delete/{id}") public String deleteBook(@PathVariable int id)
 * { // repository.deleteById(id); bookService.deleteById(id); return
 * "Book Deleted with id :" + id;
 * 
 * }
 * 
 * @DeleteMapping("/deletebook/{name}") public String deletebyBook(@PathVariable
 * String name) { bookService.deletebookByName(name);
 * 
 * return "Book Deleted with name :" + name;
 * 
 * } }
 */